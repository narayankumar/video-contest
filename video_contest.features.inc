<?php
/**
 * @file
 * video_contest.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function video_contest_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function video_contest_node_info() {
  $items = array(
    'video_contest' => array(
      'name' => t('Video Contest'),
      'base' => 'node_content',
      'description' => t('An editor-generated Contest for submission of original video entries from registered users of site'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'video_contest_entry' => array(
      'name' => t('Submit your Video'),
      'base' => 'node_content',
      'description' => t('A single video as entry to a Video Contest by a registered user'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => t('<span class="red">By submitting an entry you agree to the Terms and Conditions as specified in the <a href="">Terms of Use</a> of this site</span>'),
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
