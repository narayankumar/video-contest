<?php
/**
 * @file
 * video_contest.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function video_contest_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create video_contest content'.
  $permissions['create video_contest content'] = array(
    'name' => 'create video_contest content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create video_contest_entry content'.
  $permissions['create video_contest_entry content'] = array(
    'name' => 'create video_contest_entry content',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any video_contest content'.
  $permissions['delete any video_contest content'] = array(
    'name' => 'delete any video_contest content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any video_contest_entry content'.
  $permissions['delete any video_contest_entry content'] = array(
    'name' => 'delete any video_contest_entry content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own video_contest content'.
  $permissions['delete own video_contest content'] = array(
    'name' => 'delete own video_contest content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own video_contest_entry content'.
  $permissions['delete own video_contest_entry content'] = array(
    'name' => 'delete own video_contest_entry content',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any video_contest content'.
  $permissions['edit any video_contest content'] = array(
    'name' => 'edit any video_contest content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any video_contest_entry content'.
  $permissions['edit any video_contest_entry content'] = array(
    'name' => 'edit any video_contest_entry content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own video_contest content'.
  $permissions['edit own video_contest content'] = array(
    'name' => 'edit own video_contest content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own video_contest_entry content'.
  $permissions['edit own video_contest_entry content'] = array(
    'name' => 'edit own video_contest_entry content',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'publish button publish any video_contest'.
  $permissions['publish button publish any video_contest'] = array(
    'name' => 'publish button publish any video_contest',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button publish any video_contest_entry'.
  $permissions['publish button publish any video_contest_entry'] = array(
    'name' => 'publish button publish any video_contest_entry',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button publish editable video_contest'.
  $permissions['publish button publish editable video_contest'] = array(
    'name' => 'publish button publish editable video_contest',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button publish editable video_contest_entry'.
  $permissions['publish button publish editable video_contest_entry'] = array(
    'name' => 'publish button publish editable video_contest_entry',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button publish own video_contest'.
  $permissions['publish button publish own video_contest'] = array(
    'name' => 'publish button publish own video_contest',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button publish own video_contest_entry'.
  $permissions['publish button publish own video_contest_entry'] = array(
    'name' => 'publish button publish own video_contest_entry',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish any video_contest'.
  $permissions['publish button unpublish any video_contest'] = array(
    'name' => 'publish button unpublish any video_contest',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish any video_contest_entry'.
  $permissions['publish button unpublish any video_contest_entry'] = array(
    'name' => 'publish button unpublish any video_contest_entry',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish editable video_contest'.
  $permissions['publish button unpublish editable video_contest'] = array(
    'name' => 'publish button unpublish editable video_contest',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish editable video_contest_entry'.
  $permissions['publish button unpublish editable video_contest_entry'] = array(
    'name' => 'publish button unpublish editable video_contest_entry',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish own video_contest'.
  $permissions['publish button unpublish own video_contest'] = array(
    'name' => 'publish button unpublish own video_contest',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish own video_contest_entry'.
  $permissions['publish button unpublish own video_contest_entry'] = array(
    'name' => 'publish button unpublish own video_contest_entry',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'site admin' => 'site admin',
    ),
    'module' => 'publish_button',
  );

  return $permissions;
}
